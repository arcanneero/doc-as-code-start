= Liste des notes

* TNT
** link:tnt/video-lan[Vidéo LAN] link:tnt/video-lan.pdf[(pdf)]
** link:tnt/js-imp-3D[BabylonJS et impression 3D] link:tnt/js-imp-3D.pdf[(pdf)]
** link:tnt/postgres[Postgres et l'anarchie organisée] link:tnt/postgres.pdf[(pdf)]
* Agile Tour Montpelier
** link:atm/4-days-week-final[La semaine de 4 jours] link:atm/4-days-week-final.pdf[(pdf)]
* DevFest Strasbourg
** link:str/zqsd-production-note[ZQSD] link:str/zqsd-production.pdf[(pdf)]
* Vélo
** link:velo/inventaire[Inventaire]
** link:velo/itineraire[Itinéraire]