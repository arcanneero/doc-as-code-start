= Inventaire


.Matériel vélo
|===
|Equipement |Transport | Pluie | Pharmacie
|Pompe à vélo+
Kit réparation +
Dériveteur
|Porte vélo 3 vélos
|Sacs jaunes
|Doliprane enfant +
Doliprane adulte

|Chambres à air +
1 28" 1.5 +
1 26" 1.75 +
1 26" 2.2 +
1 20" +
1 16"
|Porte vélé 2 vélos
|Kits saccoches
|Désinfectant +
Pensemants +
Compresse +
Ciseaux

|WD40
|
|
|Thermomètre +
Pince à échardTransport e

|Antivol (*)
|
|
|Arnica (gel/tube) +
Cicatryl +
Apaisyl

|Tendeurs (5)
|
|
|Citronelle

|Powerbank +
Cables +
Quick charge
|
|
|Ventoline +
Bécotide +
Chambre

|
|
|
|Bande +
Stripe

|
|
|
|Crème solaire
|===

<<<

.Matériel camping
|===
|Nuit |Repas |Lavages | Utile

|2 secondes
|Gaz (MN/CF)
|Fil à linge (D) +
Epingles (*)
|tire bouchon - décapsuleur (NM)

|Quickhicker 3
|Réserve Gaz
|Bouche évier (NM) +
Bassine (NM)
| Sacs poubelles

|Quickhicker 4
|Saladier pliable (D)
|Lessive (D) +
Produit vaiselle (NM) +
Eponges (*) +
Torchon
|Papier toilette (2)

|5 matelas (NM) +
 2 matelas (CF) +
 1 matelas (D)
|Popote x2 (NM) +
 Popote x? (CF)
|Brosse Lessive (NM)
|Bache (NM)

|3 compacts (NM)+
 1 turbulette (NM) +
 3 normaux (CF) +
 1 normal (D)
| Couverts (*) +
Econome
|Filtre thé
|Mouchoirs

|Lampes (*)
|Alumettes ou briquet
|
|

|Maillet (CF)
|
|
|

|
|Planche à découpée
|
|

|
|Sac isoterme (NM) +
Glacons (CF)
|
|
|===

<<<

.Denrées
|===
|Salée|Sucrée|Stock|Liquide
|Sel / épices
|Confiture
|Pâtes
|Café

|Noix
|Petit dej +
Madelaine
|Couscous
|Thé / Tisane

|Légumes
|Compotes +
Yahourt à boire
|Riz
|Lait

|Cubes
|Fruits sec
|Nouilles chinoise
|Jus de fruit

|Huile vinaigre
|Sucre
|
|

|
|Chocolat poudre
|
|

|
|Barres céréales
|
|
|===
