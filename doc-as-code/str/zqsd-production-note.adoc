ZQSD Production - l'envers du décors
Par Timothée Malossane

Directeur technique, issue du monde du dev

Comment on développe de l'infra sur de l'événementiel

Contexte

Des événements en direct
Vivre pour de l'éphémère
Pas le droit à l'erreur (visible et exposé)

Timothée, Son parcours
Parcours dev
Passion jeu
Rejoint l'eSport
Participe à la créatino de zqsd

Directeur Technique, C'est s'assurer du fonctionnement de toute l'infrastructure physique (Réseau, électricité) Avant et pendant l'événement

Une réponse à la question : Comment utiliser le dev pour alimenter les éléments tech (lumières, écrans) avec de la data en provenance du jeu ?

L'évenmentiel dans l'esport

28M de personne
12M de l'esport

Leur travail consiste à reprise le HUD pour améliorer l'UX.
Dans l'événementiel on utilise de Charter generator pour incruster des éléments.
Ils repartent de ces principes et en font autre chose.

Avec une stack basée sur NodeJS, transit d'éléments en JSON pour passer de l'info, et des systèmes basé sur des événements pour déclenceher des actions (MQTT par ex).

Au-delà du HUD, les  éléments sont repris pour un affichage en salle avec des looks très différents (plus impacts sur éclairage par exemple).

=> Réorganisation des informations pour adapter le message

La plateforme

Centralisation des informations
Vidéo sur résea pour limiter les câblages
Système événementiel pour s'adapter aux flux
* WebRTC pour avoir des performances acceptable (sur le web)

On affiche que ce qui ne contient AUCUNE erreur.

=> ce qui n'est pas transmis n'est pas connu
=> ce qui est transmis avec erreur génère de la frustration

Aléas

Climatique (eau)
Travailler sous la pression (mode fire)
Technique (backups down)
S'adapter à l'imprévu, et aux demandes "métier" (alain chabat)

Exemple sur des productions

Zevent

Apport de modules spécifiques
System de Cagnotte
ZPlace
HUD spécifique à l'événement

Bercy

Séquencer de 800 machines lumière et écrans led
Séquences type messaging pour l'enchainement d'action

ZLan

Format très aléatoire
Event le plus complexe
Jeux confidentiels
Collecte d'info
Lecture mémoire
Reconnaissance d'image

La lecture mémoire est associé à de la triche, c'est donc de plus en plus difficile, et donc le traitement d'image est souvent plus simple pour extraire de la data.

Bonus

Mise en place de TDB de métrique

Réseau => peu de débit, une stabilité absolue
Lan cache => Priorité aux données de LAN vs MAJ, flux vidéo, etc..
Électricité => Stabilité réseau


